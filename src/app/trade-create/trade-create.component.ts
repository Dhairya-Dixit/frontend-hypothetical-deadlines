import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TradeService } from "../shared/tradeserv.service";

@Component({
  selector: 'app-trade-create',
  templateUrl: './trade-create.component.html',
  styleUrls: ['./trade-create.component.css']
})
export class TradeCreateComponent implements OnInit {

  

  @Input() tradeDetails = { id: 0,  ticker:'',price:0, volume:0, buy_or_sell: ''}

  constructor(
    public tradeApi: TradeService, 
    public router: Router
  ) { }

  ngOnInit() { }

  addTrade() {
    this.tradeApi.createTrade(this.tradeDetails).subscribe((data: {}) => {
      this.router.navigate(['/trade-list'])
    })
  }

}
