import { Component, OnInit } from '@angular/core';
import { TradeService } from "../shared/tradeserv.service";

@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  TradeModel: any = [];
  
  constructor( 
    public restApi: TradeService
    ) { }

  ngOnInit(): void {
    this.loadTrades()
  }

  loadTrades() {
    return this.restApi.getTrades().subscribe((data: {}) => {
        this.TradeModel = data;
    })
  }

  deleteTrade(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteTrade(id).subscribe(data => {
        this.loadTrades()
      })
    }
  }  

}

