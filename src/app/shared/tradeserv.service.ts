import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TradeModel } from '../shared/trade-model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TradeService {
  tradeapiURL = 'http://localhost:8081/api/tradehistory/';

  constructor(private http: HttpClient) 
  { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  
  getTrades(): Observable<TradeModel> {
    return this.http.get<TradeModel>(this.tradeapiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  createTrade(trade:TradeModel): Observable<TradeModel> {
    return this.http.post<TradeModel>(this.tradeapiURL, JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  
  updateTrade(id:number, trade:TradeModel): Observable<TradeModel> {
    return this.http.put<TradeModel>(this.tradeapiURL, JSON.stringify(trade), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  deleteTrade(id:number){
    return this.http.delete<TradeModel>(this.tradeapiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  
}

