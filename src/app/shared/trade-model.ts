export class TradeModel {
    constructor(){
        this.id=0;
        this.ticker="";
        this.price=0;
        this.volume=0;
        this.buy_or_sell="";
        //this.status_code=0;
       // this.trade_time= new Date();

    }
    id: number;
    ticker: string;
    price: number; 
    volume: number;
    buy_or_sell: string;
  //  status_code: number;
  //  trade_time: Date;
}
