import { TestBed } from '@angular/core/testing';

import { TradeservService } from './tradeserv.service';

describe('TradeservService', () => {
  let service: TradeservService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TradeservService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
