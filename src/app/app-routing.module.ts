import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TradeCreateComponent } from './trade-create/trade-create.component';
import { TradeListComponent } from './trade-list/trade-list.component';
import { TradeUpdateComponent } from './trade-update/trade-update.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'trade-list' },
  { path: 'trade-create', component: TradeCreateComponent },
  { path: 'trade-list', component: TradeListComponent },
  { path: 'trade-update/:id', component: TradeUpdateComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
